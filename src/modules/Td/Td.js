import BaseElement from '../BaseElement'
import './Td.scss'

class Td extends BaseElement{
	constructor(elem) {
		super()
		this.element = elem
	}
}

export default Td