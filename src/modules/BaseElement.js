class DOMElement {
	constructor(elem) {
		this.element = elem
	}

	get element() {
		return this._elem
	}

	set element(value) {
		this._elem = document.createElement(value)
	}

	addCSSClass(CSSClass) {
		this.element.classList.add(CSSClass)
	}

}

export default DOMElement