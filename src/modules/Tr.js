import BaseElement from './BaseElement'
import Td from './Td/Td'

class Tr extends BaseElement{
	constructor(elem) {
		super()
		this.element = elem
	}

	createTableData(node = '') {
		const td = new Td('td')
		this.element.append(td.element)
		td.element.innerHTML = node;
		return td
	}
}

export default Tr