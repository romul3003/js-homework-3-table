import BaseElement from '../BaseElement'
import Tr from '../Tr'
import './Table.scss'

class Table extends BaseElement {
	constructor(element) {
		super()
		this.element = element
		this._tableRows = []
	}

	createTableRow() {
		const tr = new Tr('tr')
		this.element.append(tr.element)
		this._tableRows.push(tr)
		return tr
	}

	getTableRows() {
		return this._tableRows
	}

	addEvent(event, callback) {
		this.element.addEventListener(event, callback);
	}
}

export default Table