import Table from './modules/Table/Table'

const table = new Table('table')
table.addCSSClass('table')
table.addCSSClass('table--highlight')

for (let i = 0; i < 30; i++) {
	table.createTableRow()
}

const tableRows = table.getTableRows()

tableRows.forEach((td) => {
	for (let i = 0; i < 30; i++) {
		td.createTableData()
	}
})

table.addEvent('click', function(event) {
	let td = event.target.closest('td')

	if (!td) return
	if (!this.contains(td)) return

	td.classList.toggle('highlight')
})

document.body.addEventListener('click', handleColorChange)

function handleColorChange(event) {
	const target = event.target

	if (target.classList.contains('table') || target.tagName === 'TD') return

	const table = this.querySelector('.table')
	table.classList.toggle('table--highlight')
}

const container = document.querySelector('.container')
container.append(table.element)
